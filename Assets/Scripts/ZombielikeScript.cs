using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumLibrary;

public class ZombielikeScript : EnemyController
{
    int hitType;
    int hitda�o;
    int deadType;
    public EnemyAnimationActions actions;
    public int postDiehp;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        deadType = Random.Range(1,3);
        gameObject.GetComponent<Animator>().SetInteger("dead",deadType);
        hitType= Random.Range(1, 4);
        gameObject.GetComponent<Animator>().SetInteger("attack", hitType);
    }

    private void Update()
    {
        switch (actions)
        {
            case EnemyAnimationActions.Idle:
                actions = EnemyAnimationActions.Walk;
                break;
            case EnemyAnimationActions.Walk:
                PatrolMovement();
                break;
            case EnemyAnimationActions.Run:
                RunMovement(GameObject.FindGameObjectWithTag("Player").gameObject);
                transform.GetChild(transform.childCount-1).gameObject.GetComponent<EnemyPlayerDetector>().CheckDistance();
                break;
            case EnemyAnimationActions.Attack:
                RunMovement(GameObject.FindGameObjectWithTag("Player").gameObject);
                transform.GetChild(transform.childCount - 1).gameObject.GetComponent<EnemyPlayerDetector>().CheckDistance();
                break;
            case EnemyAnimationActions.Die:break;
            case EnemyAnimationActions.PostDie:
                RunMovement(GameObject.FindGameObjectWithTag("Player").gameObject);
                break;

        }
    }
    public void AttackAndChange()
    {
        AttackToPlayer();
        ChangehitTypeAnd();
    }
    public void ChangehitTypeAnd()//aplicar al final de la animaci�n de hit
    {
        hitType = Random.Range(1, 4);
        gameObject.GetComponent<Animator>().SetInteger("attack", hitType);
    }
    public void AttackToPlayer()
    {
        
    }
    public void ChangeActionsType(EnemyAnimationActions accio)
    {
        if(actions!=EnemyAnimationActions.Idle)
        actions = accio;
    }
    public void GiveDamage(int damage)
    {
        if (actions!=EnemyAnimationActions.Die&&actions!=EnemyAnimationActions.PostDie) { hp -= damage;
            GetComponent<Animator>().Play("Hit");
            if (hp <= 0) { 
                GetComponent<Animator>().SetBool("live", false);
                actions = EnemyAnimationActions.Die;
            }
        }
        else
        {
            postDiehp -= damage;
            if (damage <= 0) GetComponent<Animator>().SetBool("PostDieLive",false);
        }
    }
}
