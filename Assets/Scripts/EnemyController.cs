using System.Collections.Generic;
using UnityEngine;
using EnumLibrary;
public class EnemyController : MonoBehaviour
{
    //This have to be in a ScriptableObject 
    public float firstDistance;
    public float secondDistance;
    public float firstStressDamage;
    public float SecondStressDamage;
    public  float ThirthStressDamage;
    private float _Proximitydamage;
    public float hp;
    public float movementSpeed;//Velocidad personage
    public float attackWaitTime;//Al atacar, tiempo de pausa entre ataques, se pondra un timer que al llegar a ese tiempo vuelva a atacar
    public bool interactable;
    public EnemyType typeEnemy;
    public float angleEnemyVision;//Opcional, poder hacer que si el enemigo ve al player vaya hacia el a partir del angulo de vision: murcielagos se guian por sonido asi que seria 360, humanoide 170...
    public List<Vector3> patrol;
    private int currentPatrolIndex = 0;
    private Rigidbody rb;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        rb.isKinematic = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("StressArea"))
        {
            var distance = (other.gameObject.transform.parent.position - gameObject.transform.position).magnitude;
            if (distance <= firstDistance&&distance>secondDistance)
            {
                _Proximitydamage = SecondStressDamage;
            }
            else if (distance <= secondDistance)
            {
                _Proximitydamage = ThirthStressDamage;
            }
            else
            {
                _Proximitydamage = firstStressDamage;
            }
        }
    }
    public void PatrolMovement()
    {
        Vector3 targetPosition = patrol[currentPatrolIndex];
        transform.LookAt(targetPosition);
        transform.Rotate(0,180,0);
        Vector3 direction = (targetPosition - transform.position).normalized;
        //rb.MovePosition(transform.position + direction * movementSpeed * Time.fixedDeltaTime);
        rb.velocity = direction * movementSpeed*2 * Time.deltaTime;

        if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
        {
            currentPatrolIndex = (currentPatrolIndex + 1) % patrol.Count;
        }

        if (currentPatrolIndex == 0 && Vector3.Distance(transform.position, patrol[0]) < 0.1f)
        {
            // si hemos vuelto al punto inicial, volvemos al primer �ndice
            currentPatrolIndex = 1;
        }
    }
    public void RunMovement(GameObject target)
    {
        Vector3 targetPosition = target.transform.position;
        transform.LookAt(targetPosition);
        transform.Rotate(0, 180, 0);
        Vector3 direction = (targetPosition - transform.position).normalized;
        //rb.MovePosition(transform.position + direction * movementSpeed * Time.fixedDeltaTime);
        rb.velocity = direction * (movementSpeed*2)*3.5f * Time.deltaTime;
    }

}
