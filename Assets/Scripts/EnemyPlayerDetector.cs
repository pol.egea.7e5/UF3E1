using System.Collections;
using System.Collections.Generic;
using EnumLibrary;
using UnityEngine;

public class EnemyPlayerDetector : MonoBehaviour
{
    //If a player was detected but enemy can't folow him, he can stay alert.
    public bool _playerDetected=false;
    private bool _playerSeeBehind = false;
    [SerializeField]
    private LayerMask toIgnore;
    [SerializeField]
    private float timeWaitingPlayer;

    private void OnTriggerStay(Collider other)
    {
        Debug.Log("Entered");
        if (other.gameObject.CompareTag("Player")&&!_playerDetected)
        {
            if (Physics.Raycast(new Ray(transform.parent.position, other.transform.position-transform.parent.position), out RaycastHit hit, 300, ~toIgnore))
            {
                Debug.Log(hit.collider.gameObject.name);
                if (hit.collider.transform.parent.gameObject==other.gameObject)
                {
                    if (Vector3.Dot(other.transform.position - transform.parent.position, transform.forward) < 0)
                    {
                        Debug.Log("I have to see behind me");
                        StartCoroutine(TimeWaitThePLayerToBeSeen(timeWaitingPlayer));
                    }
                    else
                    {
                        
                        Debug.Log("Player detected in front of me");
                        _playerDetected = true;
                        transform.parent.gameObject.GetComponent<ZombielikeScript>().actions = EnemyAnimationActions.Run;
                        transform.parent.gameObject.GetComponent<Animator>().SetBool("find", _playerDetected);

                    }
                }
            }
            else Debug.Log("Descarta");

        }
    }
    public void CheckDistance()
    {
        if (Physics.Raycast(new Ray(transform.parent.position, GameObject.FindGameObjectWithTag("Player").transform.position - transform.parent.position), out RaycastHit hit, 300, ~toIgnore))
        {
            Debug.Log(hit.collider.gameObject.name);
            if (hit.collider.transform.parent.gameObject == GameObject.FindGameObjectWithTag("Player").gameObject)
            {
                if (hit.distance <= 3)
                {
                    Debug.Log("Enemy to hit");
                    transform.parent.gameObject.GetComponent<ZombielikeScript>().actions = EnemyAnimationActions.Attack;
                    transform.parent.gameObject.GetComponent<Animator>().SetBool("near", true);
                }
                else
                {
                    transform.parent.gameObject.GetComponent<Animator>().SetBool("near", false);
                }
            }
        }
    }


    private void OnTriggerExit(Collider other)
    {
       if(_playerDetected)
        {
          Debug.Log("PlayerLost");
          _playerDetected = false;
            _playerSeeBehind = false;
            transform.parent.gameObject.GetComponent<ZombielikeScript>().actions = EnemyAnimationActions.Idle;
            transform.parent.gameObject.GetComponent<Animator>().SetBool("find", _playerDetected);
        }
    }

    private IEnumerator TimeWaitThePLayerToBeSeen(float time)
    {
        _playerSeeBehind = true;
        yield return new WaitForSeconds(time);
        _playerSeeBehind = false;
    }
}

