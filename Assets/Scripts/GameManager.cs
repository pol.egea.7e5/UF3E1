using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject coins;
    public int coinscounter = 0;
    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.childCount == 0)
        {
            Instantiate(coins, new Vector3(Random.Range(27,-10), -6.75f, Random.Range(30,-10)), new Quaternion(0, 0, 0, 0), transform);
        }
    }
}
