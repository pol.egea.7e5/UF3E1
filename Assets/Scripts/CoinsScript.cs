using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsScript : MonoBehaviour
{
    // Start is called before the first frame update

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player" && GameManager.Instance != null) GameManager.Instance.coinscounter += 1;
        if (other.name != "Ground" || other.name != "Ground_Mesh") Destroy(this.gameObject);
    }
}
